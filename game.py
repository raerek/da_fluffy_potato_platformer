import sys
import pygame



class Game():
    def __init__(self):
        pygame.init()
        pygame.display.set_caption('ninja game')
        self.screen = pygame.display.set_mode((640,480)) # félrevezető a neve, valójában ez hozza létre az ablakot
        self.clock = pygame.time.Clock() # azért kell, mert így majd tudunk 60FPS-szel működni, és nem lesz olyan gyors a program, amilyen csak lehet
        self.img = pygame.image.load('data/images/clouds/cloud_1.png')
        self.img.set_colorkey((0, 0, 0)) # a felhő képe fekete hátterű, ezzel mondjuk meg, hogy a fekete legyen átlátszó
        self.img_pos = [160, 260]
        self.movement = [False, False]
        self.collision_area = pygame.Rect(50, 50, 300, 50) # ezzel fog ütközni az img_r

    def run(self):
        while True:
            self.screen.fill((14, 219, 248)) # minden kör előtt képernyőt törlünk, hogy a mozgások ne hagyjanak nyomot; ez azzal jár, hogy újra kell rajzolni az egész képernyőt
            self.img_r = pygame.Rect(self.img_pos[0], self.img_pos[1], self.img.get_width(), self.img.get_height()) # image_rectangle, ez fog ütközni a collision area-val
            if self.img_r.colliderect(self.collision_area):
                pygame.draw.rect(self.screen, (0, 100, 255), self.collision_area) # más színű a téglalap, ha nekimegyünk és más, ha nem
            else:
                pygame.draw.rect(self.screen, (0, 50, 155), self.collision_area)
            self.img_pos[1] += (self.movement[1] - self.movement[0]) * 5 # az egyszerre lenyomásokat kezeljük a kivonással, a szorzás pedig a mozgás sebességét változtatja
            self.screen.blit(self.img, self.img_pos) # a self.screen-t az init()-ben megadtuk; azért kerül ez a rész a téglalap *után*, mert akkor a téglalap elé kerül a felhő és nem mögé

            for event in pygame.event.get():
                if event.type == pygame.QUIT: # az ablak bezárása
                    pygame.quit()
                    sys.exit()
                if event.type == pygame.KEYDOWN: # lenyomtak egy gombot
                    if event.key == pygame.K_UP: # csak a lenyomás és a felengedés, a nyomva tartás nem
                        self.movement[0] = True
                    if event.key == pygame.K_DOWN:
                        self.movement[1] = True
                if event.type == pygame.KEYUP: # felengedtek egy gombot
                    if event.key == pygame.K_UP: # csak a lenyomás és a felengedés, a nyomva tartás nem
                        self.movement[0] = False
                    if event.key == pygame.K_DOWN:
                        self.movement[1] = False

            pygame.display.update() # a változtatások mindíg az update() hívásakor kerülnek képernyőre
            self.clock.tick(60) # 60FPS, ez a függvény lényegében egy dinamikus sleep(): a két tick közötti igazi ténykedést kipótolja annyival, ami még kell a 60FPS-hez


Game().run()