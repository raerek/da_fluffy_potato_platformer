# DaFluffyPotato platformer

- [ez alapján](https://www.youtube.com/watch?v=2gABYM5M0ww)
- [git ez alapján](https://blog.mergify.com/feature-branch-a-quick-walk-through-git-workflow/), a videó egy-egy szekciója egy-egy feature branch a git-ben

## VS Code
- ha a Pylint szerint "module pygame has no init member", akkor csinnáld [ezt](https://stackoverflow.com/questions/50569453/why-does-it-say-that-module-pygame-has-no-init-member)

## Fogalmak, amik nem derülnek ki a forráskódból
- A *surface* olyan fogalom, amire rá lehet `.blit()`-ni. Ilyen surface a `self.screen`, de ilyen pl a `self.img` is. Ha valamit ráblittelünk, az ráíródik, nem objektum lesz. Ez azzal jár, hogy ha azt a valamit mozhatjuk, akkor a mostani helyén is ott marad, nem *mozog*.

